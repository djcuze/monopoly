class Seed

  def self.players
    # (name, token, funds, position_on_board, in_jail?)
    [
        Player.new('Nate', 'Iron', 1500, 1),
        Player.new('John', 'Dog', 1500, 1),
        Player.new('Scott', 'Car', 1500, 1),
        Player.new('Joe', 'Battleship', 1500, 1)
    ]
  end

  def self.properties
    #( name, Cost, Colour Group, Mortgage Value, Rent, Board_position)
    [
        Property.new('Old Kent Road', 60, 'brown', 30, 2, 2),
        Property.new('Whitechapel Road', 60, 'brown', 30, 4, 4),
        Property.new('The Angel Islington', 100, 'light_blue', 50, 6, 7),
        Property.new('Euston Road', 100, 'light_blue', 50, 6, 9),
        Property.new('Pentonville Road', 120, 'light_blue', 60, 8, 10),
        Property.new('Pall Mall', 140, 'pink', 70, 10, 12),
        Property.new('Whitehall', 140, 'pink', 70, 10, 14),
        Property.new('Northumberland Avenue', 160, 'pink', 80, 12, 15),
        Property.new('Bow Street', 180, 'orange', 90, 14, 17),
        Property.new('Marlborough Street', 180, 'orange', 90, 14, 19),
        Property.new('Vine Street', 200, 'orange', 100, 16, 20),
        Property.new('Strand', 220, 'red', 110, 18, 22),
        Property.new('Fleet Street', 220, 'red', 110, 18, 24),
        Property.new('Trafalgar Square', 240, 'red', 120, 20, 25),
        Property.new('Leicester Square', 260, 'yellow', 130, 22, 27),
        Property.new('Coventry Street', 260, 'yellow', 130, 22, 28),
        Property.new('Piccadilly', 280, 'yellow', 140, 24, 30),
        Property.new('Regent Street', 300, 'green', 150, 26, 32),
        Property.new('Oxford Street', 300, 'green', 150, 26, 33),
        Property.new('Bond Street', 320, 'green', 160, 28, 35),
        Property.new('Park Lane', 350, 'dark_blue', 175, 35, 38),
        Property.new('Mayfair', 400, 'dark_blue', 200, 50, 40)
    ]
  end

  def self.railroads
    [
        Railroad.new('Kings Cross Station', 200, 100, 25, 6),
        Railroad.new('Marylebone Station', 200, 100, 25, 16),
        Railroad.new('Fenchurch St Station', 200, 100, 25, 26),
        Railroad.new('Liverpool St Station', 200, 100, 25, 36)
    ]
  end

  def self.cards
    # Board Position, Name, Type
    [
        Card.new(3, 'Community Chest'),
        Card.new(8, 'Chance'),
        Card.new(18, 'Community Chest'),
        Card.new(23, 'Chance'),
        Card.new(34, 'Community Chest'),
        Card.new(37, 'Chance')
    ]
  end

  def self.utility
    # Name, Cost, Mortgage Value, Board Position
    [
        Utility.new('Electricity Company', 150, 75, 13),
        Utility.new('Water Works', 150, 75, 29)
    ]
  end
end