require 'models/tax'

RSpec.describe Tax::Income do
  let(:nate) { Player.new('Nate', 'Iron', 1500, 1) }
  let(:tax) { Tax::Income.new }

  describe '.pay' do
    context 'when the player has the funds to pay and chooses to pay 200' do
      it "subtracts the correct amount from the player's funds" do
        nate_initial_funds = 1500
        tax.estimate(nate)
        expect(nate.funds).to eq(nate_initial_funds - 200)
      end
    end
    context 'when the player cannot afford to pay' do
      it 'gives an error' do
        nate.funds = 0
        expect { tax.estimate(nate) }.to raise_error(StandardError)
      end
    end
  end

  describe '.estimate' do
    context 'when a player has enough funds and chooses to calculate 10% of their assets' do
      it "subtracts the correct amount from the player's funds" do
        nate_initial_funds = 1500
        nate_initial_assets = 1500
        tax.calc_percent(nate)
        expect(nate.funds).to eq(nate_initial_funds - (nate_initial_assets*0.1).round(0))
      end
    end
  end

end
