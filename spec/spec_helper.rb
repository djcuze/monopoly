require 'pathname'
SPEC_ROOT = Pathname(__dir__)

Dir[
  SPEC_ROOT.join('fixtures/**/*.rb'),
  SPEC_ROOT.join('support/**/*.rb')
].each(&method(:require))

RSpec.configure do |config|
  config.disable_monkey_patching!
end
