RSpec.describe Property do
  let(:property) { Property.new('Whitechapel Road', 60, 'brown', 30, 4, 3) }
  let(:property2) { Property.new('Old Kent Road', 60, 'brown', 30, 2, 2) }
  let(:property3) { Property.new('Mayfair', 400, 'dark_blue', 200, 50, 40) }
  let(:nate) { Player.new('Nate', 'Iron', 1500, 1) }
  let(:john) { Player.new('John', 'Iron', 1500, 1) }

  describe '#purchase' do

    context 'when player has enough funds to buy' do
      it "adds the property to the player's owned properties and subtracts the cost from their funds" do
        nate_initial_funds = nate.funds
        Buy.call(nate, property)
        expect(nate.funds).to eq(nate_initial_funds - property.cost)
        expect(nate.owned_properties).to include(property)
      end
    end

    context "when player doesn't have enough funds to buy" do
      before(:each) do
        nate.funds = 0
      end
      it 'gives an error' do
        expect { Buy.call(nate, property) }.to raise_error(StandardError)
      end
    end
  end

  describe '#mark_as_complete' do
    it 'marks the property as completed' do
      expect(property).not_to be_completed
      property.mark_as_complete
      expect(property).to be_completed
    end
  end

  describe '#rent' do
    context 'when the property set is complete' do
      it 'expects the rent due to be doubled' do
        property_initial_rent = property.rent
        property.mark_as_complete
        expect(property.rent).to eq(property_initial_rent * 2)
      end
    end
  end

  describe '#no_of_houses' do

    context 'when a house is bought on a property' do
      it 'expects the no_of_houses to increase by one' do
        property.buy_house(nate)
        expect(property.no_of_houses).to eq(1)
      end
    end

    context "when a house isn't bought on a property" do
      it 'expects the no_of_houses to be 0' do
        expect(property.no_of_houses).to eq(0)
      end
    end
  end

  describe '#house_price' do

    context 'when the property is in the brown group' do
      it 'expects the cost to purchase a house to equal 50' do
        expect(property.house_price).to eq(50)
      end
    end

    context 'when the property is in the dark_blue group' do
      it 'expects the cost to purchase a house to equal 200' do
        expect(property3.house_price).to eq(200)
      end
    end
  end

  describe '#hotel?' do
    context 'when a property has 4 houses and a hotel is purchased' do
      before(:each) do
        property.no_of_houses = 4
      end
      it 'expects the property to flag #hotel as true' do
        expect(property.no_of_houses).to eq(4)
        property.buy_house(nate)
        expect(property.hotel?).to eq(true)
        expect(property.no_of_houses).to eq(0)
      end
    end
  end

  describe '#rent' do
    context 'when Old Kent Road has 1 house' do
      it 'expects the rent to equal 20' do
        property2.buy_house(nate)
        expect(property2.no_of_houses).to eq(1)
        expect(property2.rent).to eq(10)
      end
    end

    context 'when Whitechapel Road has 1 house' do
      it 'expects the rent to equal 10' do
        property.buy_house(nate)
        expect(property.rent).to eq(20)
      end
    end

    context 'when Mayfair has a hotel' do
      it 'expects the rent to equal 2000' do
        property3.buy_hotel(nate)
        expect(property3.rent).to eq(2000)
      end
    end

  end
end
