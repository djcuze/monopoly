require 'actions/complete_set'

RSpec.describe CompleteSet do
  let(:properties) do
    [
        Property.new('Whitechapel Road', 60, 'brown', 30, 4, 3),
        Property.new('Old Kent Road', 60, 'brown', 30, 2, 2)
    ]
  end
  let(:player) { double('Player') }

  context 'when all properties are owned by the same player' do
    before(:each) do
      properties.each do |property|
        property.bought_by(player)
      end
    end

    it 'marks all properties as complete' do
      CompleteSet.call(properties)
      expect(properties).to all(be_completed)
    end
  end

  context 'when not all properties are owned by the same player' do
    before(:each) do
      properties.first.bought_by(player)
    end

    it 'does not mark all properties as complete' do
      CompleteSet.call(properties)
      expect(properties).to all(be_incomplete)
    end
  end
end