require 'models/utility'

RSpec.describe Utility do
  let(:utility) { Utility.new('Electricity Company', 150, 75, 13) }
  let(:utility2) { Utility.new('Water Works', 150, 75, 29) }
  let(:nate) { Player.new('Nate', 'Iron', 1500, 1) }
  let(:john) { Player.new('John', 'Battleship', 1500, 1) }

  describe '#rent' do
    before(:each) do
      utility.bought_by(john)
    end

    context 'When a single utility square is owned and a player can afford to pay' do
      it "calculates its rent to be 4 times the result of the dice roll and subtracts this from the player's funds" do
        roll = Roll.new
        nate_initial_funds = nate.funds
        john_initial_funds = john.funds
        expect(utility).not_to be_completed
        expect(utility.rent(roll)).to eq(4 * roll.result)
        Rent.pay(nate, john, utility.rent(roll))
        expect(nate.funds).to eq(nate_initial_funds - 4*roll.result)
        expect(john.funds).to eq(john_initial_funds + 4*roll.result)
      end
    end

    context 'When both utilities are owned by a player' do
      before(:each) do
        utility.bought_by(john)
        utility2.bought_by(john)
        utility.mark_as_complete
        utility.mark_as_complete
      end

      it 'expects its rent to be 10 times the dice roll result' do
        roll = Roll.new
        nate_initial_funds = nate.funds
        john_initial_funds = john.funds
        expect(utility).to be_completed
        expect(utility.rent(roll)).to eq(10 * roll.result)
        Rent.pay(nate, john, utility.rent(roll))
        expect(nate.funds).to eq(nate_initial_funds - 10*roll.result)
        expect(john.funds).to eq(john_initial_funds + 10*roll.result)
      end
    end
  end

  describe '#mark_as_complete' do
    it 'marks the property as completed' do
      expect(utility).not_to be_completed
      utility.mark_as_complete
      expect(utility).to be_completed
    end
  end
end