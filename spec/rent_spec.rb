require 'models/rent'

RSpec.describe Rent do
  let(:property) { Property.new('Whitechapel Road', 60, 'brown', 30, 4, 3) }
  let(:nate) { Player.new('Nate', 'Iron', 1500, 1) }
  let(:john) { Player.new('John', 'Dog', 1500, 1) }
  before(:each) do
    property.bought_by(john)
  end

  describe '.pay' do
    context 'when paying player has enough funds' do
      it 'Transfers the correct amount of funds in accordance with the rent value to the appropriate players' do
        nate_initial_funds = nate.funds
        john_initial_funds = john.funds
        Rent.pay(nate, john, property.rent)
        expect(nate.funds).to eq(nate_initial_funds - property.rent)
        expect(john.funds).to eq(john_initial_funds + property.rent)
      end
    end

    context "when paying player doesn't have enough funds" do
      before(:each) do
        nate.funds = 0
      end
      it "Gives an error if a player doesn't have the required funds to pay due rent" do
        expect { Rent.pay(nate, john, property.rent) }.to raise_error(StandardError)
      end
    end
  end
end
