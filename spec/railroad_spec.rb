require 'models/railroad'

RSpec.describe Railroad do
  let(:railroad) { Railroad.new('Kings Cross Station', 200, 100, 25, 6) }
  let(:nate) { Player.new('Nate', 'Iron', 1500, 1) }

  describe '#purchase' do
    context 'when a player has enough funds to buy' do
      it "adds the property to the player's owned properties and subtracts the cost from their funds" do
        nate_initial_funds = 1500
        Buy.call(nate, railroad)
        expect(nate.owned_properties).to include(railroad)
        expect(nate.funds).to eq(nate_initial_funds - railroad.cost)
      end
    end
    context "when a player doesn't have the required funds" do
      before(:each) do
        nate.funds = 0
      end
      it 'gives an error' do
        expect { Buy.call(nate, railroad) }.to raise_error(StandardError)
      end
    end
  end

  describe '#mark_as_complete' do
    it 'marks the property as completed' do
      expect(railroad).not_to be_completed
      railroad.mark_as_complete
      expect(railroad).to be_completed
    end
  end
end