require 'models/player'
require 'models/property'
require 'actions/buy'
require 'colorize'

RSpec.describe Player do
  let(:property) { Property.new('Whitechapel Road', 60, 'brown', 30, 4, 3) }
  let(:property2) { Property.new('Old Kent Road', 60, 'brown', 30, 2, 2) }
  let(:nate) { Player.new('Nate', 'Iron', 1500, 1) }

  describe '#total_assets' do
    before(:each) do
      property.purchase(nate)
      property2.purchase(nate)
      nate.funds = 1500
    end
    context 'when player owns two properties' do
      it "combines the properties' values with the player's funds to equal the total assets of the player" do
        expect(nate.total_assets).to eq(nate.funds + property.cost + property2.cost)
      end
    end
  end

  describe '#move' do
    context 'When a player lands on the Go To Jail! square' do
      before(:each) do
        nate.position_on_board = 29
      end
      it 'sends the player to jail' do
        spaces = 2
        board_total = 40
        nate.move(spaces, board_total)
        expect(nate.position_on_board).to eq(10)
      end
    end

    context 'When a player does a circuit of the board' do
      before(:each) do
        nate.position_on_board = 38
      end
      it "adds 200 to the player's funds" do
        initial_funds = nate.funds
        spaces = 4
        board_total = 40
        nate.move(spaces, board_total)
        expect(nate.funds).to eq(initial_funds + 200)
        expect(nate.position_on_board).to eq(2)
      end
    end
  end
end