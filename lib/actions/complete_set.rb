class CompleteSet

  def self.call(properties)
    owners = properties.collect(&:owned_by_player).uniq
    properties.each(&:mark_as_complete) if owners.none?(&:nil?) && owners.length == 1
  end

end