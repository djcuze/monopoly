class Roll
  attr_accessor :result
  attr_reader :rolled_doubles

  def initialize
    @rolled_doubles = false
    first_roll = rand(6) + 1
    second_roll = rand(6) + 1
    @result = first_roll + second_roll
    @rolled_doubles = first_roll == second_roll
  end

end