class Buy

  def self.call(player, property)
    if player.funds >= property.cost
      player.buy_property(property)
      property.bought_by(player)
    else
      raise StandardError
    end
  end

end