class Turn
  attr_accessor :current_player_index
  attr_reader :board

  def initialize(current_player_index, board)
    @ended = false
    @current_player_index = current_player_index
    @current_player = board.players[current_player_index.to_i]
    @consecutive_turns = 1
    @board = board
  end

  def start
    player = @current_player
    player.display_info(board)
    process(player)
    end_turn
  end

  def process(player)
    @consecutive_turns += 1
    request_roll
    roll = roll_dice
    player.move(roll.result, board.squares.length)
    location_type(player)
    if roll.rolled_doubles
      puts "You've rolled doubles!".colorize(:color => :green)
      rolled_three_doubles? ? player.send_to_jail : process(player)
    end
  end

  def rolled_three_doubles?
    @consecutive_turns >= 3
  end

  def location_type(player)
    location = board.squares[player.position_on_board]
    puts "You've landed on #{location.name}"
    if location.is_property?
      property_decision(location)
    elsif location.is_tax?
      puts 'This is a tax tile!'
      location.pay(player)
    elsif location.is_card?
      puts "You've landed on a card space!\nI haven't written the code for this yet so here's $50!"
      player.funds += 50
    end
  end

  def property_decision(property)
    player = @current_player
    if property.for_sale?
      purchase_request(property)
    else
      puts "Uh-oh! #{property.name} is already owned by #{property.owned_by_player.name}\nThey collect $#{property.rent} rent from you\n\n"
      begin
        Rent.pay(player, property.owned_by_player, property.rent) if property.is_owed_rent?(player, property)
      rescue StandardError
        puts "Uh-oh! #{player.name} does not have enough funds to pay rent of #{property.rent}!\n"
      end
    end
  end

  def end_turn
    player = @current_player
    puts "\nYou have " + "$#{player.funds}".colorize(:color => :green) + " remaining\n\n"
    @ended = true
  end

  def ended?
    @ended
  end

  def play
    start
  end

  def request_roll
    rsp = ''
    until rsp == 'roll'
      puts "type 'roll' into the console to begin your turn"
      rsp = gets.chomp
    end
  end

  def purchase_request(property)
    rsp = ''
    until rsp == 'y' || rsp == 'n'
      puts "Would you like to purchase #{property.name} for $#{property.cost}? y/n"
      rsp = gets.chomp
    end
    if rsp == 'y'
      begin
        property.purchase(@current_player)
        CompleteSet.call(board.property_group(property.colour_group))
        puts "\n#{property.name} now belongs to #{@current_player.name}\n"
      rescue StandardError
        puts "Uh-oh! It seems you don't have enough funds to purchase"
      end
    elsif rsp == 'n'
      puts "You've chosen not to purchase the property"
    end
  end

  def roll_dice
    roll = Roll.new
    puts "\nYou rolled for: #{roll.result.to_s}"
    roll
  end
end