require_relative '../bin/require'

class Game
  attr_accessor :current_player_index
  attr_reader :board

  def initialize
    @board = Board.new
    @board.players += Seed.players
    @board.utilities += Seed.utility
    @board.railroads += Seed.railroads
    @board.properties += Seed.properties
    @board.cards += Seed.cards
    @board.corners += [
        Corner::PassGo.new,
        Corner::Jail.new,
        Corner::GoToJail.new,
        Corner::FreeParking.new
    ]
    @board.tax += [
        Tax::Income.new,
        Tax::Luxury.new
    ]
    @current_player_index = 0
  end

  def change_current_player
    if @current_player_index == board.players.length - 1
      @current_player_index = 0
    else
      @current_player_index += 1
    end
  end

  def run
    puts "The game has begun!\n\n"
    until ended? do
      turn = Turn.new(@current_player_index, board)
      process(turn)
      change_current_player
    end
  end

  def process(turn)
    until turn.ended? do
      turn.play
    end
  end

  def ended?
    board.players.length == 0
  end
end