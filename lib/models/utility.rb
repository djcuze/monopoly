require_relative '../actions/roll'
class Utility
  attr_accessor :name, :cost, :mortgage_value, :rent, :board_position, :owned_by_player, :completed

  def initialize(name, cost, mortgage_value, board_position)
    @name = name
    @cost = cost
    @mortgage_value = mortgage_value
    @for_sale = true
    @completed = false
    @owned_by_player = nil
    @completed = false
    @board_position = board_position
  end

  def bought_by(player)
    @owned_by_player = player
    @for_sale = false
  end

  def purchase(player)
    Buy.call(player, self)
  end

  def is_owed_rent?(player, property)
    player != property.owned_by_player
  end

  def for_sale?
    @for_sale
  end

  def is_property?
    false
  end

  def is_utility?
    true
  end

  def is_tax?
    false
  end

  def is_card?
    false
  end

  def mark_as_complete
    @completed = true
  end

  def completed?
    @completed
  end

  def incomplete?
    !completed?
  end

  def rent(roll)
    if @completed
      @rent = (roll.result*10)
    else
      @rent = (roll.result*4)
    end
  end

end