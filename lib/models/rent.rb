class Rent

  def self.pay(payer, payee, amount)
    if payer.funds >= amount
      payer.pay_player(payee, amount)
    else
        raise StandardError
    end
  end
end