class Player
  attr_accessor :name, :token, :funds, :position_on_board, :in_jail, :combined_assets
  attr_reader :owned_properties

  def initialize(name, token, funds, position_on_board)
    # Setting Instance Variable Values to Local Variables
    @name = name
    @token = token
    @funds = funds
    @position_on_board = position_on_board
    @in_jail = false
    @owned_properties = []
    @completed_sets = []
  end

  def display_info(board)
    puts "Current Player Details:".colorize(:color => :light_white, :background => :black)
    puts "#{@name}".colorize(:green) + ' | Funds avail.' + " $#{@funds}".colorize(:green) + ' | Current Position: '+ "#{board.squares[@position_on_board - 1].name}".colorize(:red) + "\n\n"
    if @owned_properties.any?
      puts "Owned Properties:".colorize(:color => :light_white, :background => :black)
      @owned_properties.each { |n| puts "#{n.name}".colorize(:black) }
    end
    puts "\n"
  end

  def buy_property(property)
    @owned_properties << property
    @funds -= property.cost
  end

  def pay_player(player_owed, amount)
    @funds -= amount
    player_owed.funds += amount
  end

  def pass_go
    @funds += 200
  end

  def move(spaces, board_total)
    initial_position = @position_on_board
    if @position_on_board + spaces > board_total
      @position_on_board += spaces - board_total
    else
      @position_on_board += spaces
    end
    @position_on_board
    send_to_jail if @position_on_board == 31
    pass_go if initial_position > @position_on_board
  end

  def send_to_jail
    puts "You've been sent to jail #{@name}!".colorize(:color => :red) + ' Your turn has now ended'
    @position_on_board = 10
    @in_jail = true
  end

  def total_assets
    combined_property_value = @owned_properties.collect(&:cost).inject(0, :+)
    @total_assets = combined_property_value + @funds
  end
end