class Tax

  class Income
    attr_reader :board_position, :name

    def initialize
      @board_position = 5
      @name = 'Income Tax'
    end

    def pay(player)
      puts "You've landed on Income Tax! Pay $200 (y) or 10% of your assets (n)? y/n"
      rsp = gets.chomp
      if rsp == 'y'
        estimate(player)
      elsif rsp == 'n'
        calc_percent(player)
      end
    end

    def estimate(player)
      if player.funds >= 200
        player.funds -= 200
        puts 'Tax.pay was called'
      else
        raise StandardError
      end
    end

    def calc_percent(player)
      tax = (0.1 * player.total_assets.to_f.round(3))
      player.funds -= tax.round(0)
    end

    def is_property?
      false
    end

    def is_tax?
      true
    end
  end

  class Luxury
    attr_reader :board_position

    def initialize
      @board_position = 39
      @name = 'Luxury Tax'
    end

    def pay(player)
      puts 'You have landed on Luxury Tax. You pay $75'
      player.funds -= 75
    end

    def is_property?
      false
    end

    def is_tax?
      true
    end
  end
end