class Card
  attr_accessor :board_position, :name

  def initialize(board_position, name)
    @board_position = board_position
    @name = name
    # Square.new(board_position, name, self)
  end

  def is_property?
    false
  end

  def is_card?
    true
  end

  def is_tax?
    false
  end
end