class Railroad
  attr_accessor :name, :cost, :mortgage_value, :rent, :board_position, :owned_by_player, :completed

  def initialize(name, cost, mortgage_value, rent, board_position)
    @name = name
    @cost = cost
    @mortgage_value = mortgage_value
    @rent = rent
    @completed = false
    @for_sale = true
    @owned_by_player = nil
    @no_owned = 0
    @board_position = board_position
  end

  def bought_by(player)
    @owned_by_player = player
    @for_sale = false
  end

  def purchase(player)
    Buy.call(player, self)
  end

  def is_owed_rent?(player, property)
    player != property.owned_by_player
  end

  def for_sale?
    @for_sale
  end

  def is_property?
    true
  end

  def mark_as_complete
    @completed = true
  end

  def completed?
    @completed
  end

  def incomplete?
    !completed?
  end

  def rent
    if @no_owned == 2
      @rent * 2
    elsif @no_owned == 3
      @rent * 3
    elsif @no_owned == 4
      @rent * 4
    else
      @rent
    end
  end

end