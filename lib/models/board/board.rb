class Board
  attr_accessor :properties, :cards, :utilities, :railroads, :corners, :tax, :players

  def initialize
    @corners = []
    @squares = []
    @properties = []
    @cards = []
    @utilities = []
    @railroads = []
    @tax = []
    @players = []
  end

  def property_group(colour)
    properties.select { |property| property.colour_group == colour }
  end

  def squares
    (properties + cards + utilities + railroads + corners + tax).sort_by(&:board_position)
  end
end