class Corner

  class PassGo
    attr_reader :board_position, :name

    def initialize
      @board_position = 1
      @name = 'Go'
    end

    def is_property?
      false
    end
    def is_tax?
      false
    end
    def is_card?
      false
    end

  end

  class Jail
    attr_reader :board_position, :name

    def initialize
      @board_position = 11
      @name = 'In Jail/Just Visiting'
    end

    def is_property?
      false
    end
    def is_tax?
      false
    end
    def is_card?
      false
    end
  end

  class FreeParking
    attr_reader :board_position, :name

    def initialize
      @board_position = 21
      @name = 'Free Parking'
    end
    def is_property?
      false
    end
    def is_tax?
      false
    end
    def is_card?
      false
    end
  end

  class GoToJail
    attr_reader :board_position, :name

    def initialize
      @board_position = 31
      @name = 'Go To Jail!'
    end
    def is_property?
      false
    end
    def is_tax?
      false
    end
    def is_card?
      false
    end
  end
end