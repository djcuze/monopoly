class Property
  attr_accessor :name, :cost, :colour_group, :mortgage_value, :rent, :board_position, :owned_by_player, :completed, :no_of_houses

  def initialize(name, cost, colour_group, mortgage_value, rent, board_position)
    @name = name
    @cost = cost
    @colour_group = colour_group
    @mortgage_value = mortgage_value
    @rent = rent
    @no_of_houses = 0
    @hotel = false
    @for_sale = true
    @completed = false
    @owned_by_player = nil
    @board_position = board_position
    @house_price = 0
  end

  def bought_by(player)
    @owned_by_player = player
    @for_sale = false
  end

  def purchase(player)
    Buy.call(player, self)
  end

  def is_owed_rent?(player, property)
    player != property.owned_by_player
  end

  def for_sale?
    @for_sale
  end

  def is_property?
    true
  end

  def mark_as_complete
    @completed = true
  end

  def completed?
    @completed
  end

  def incomplete?
    !completed?
  end

  def rent

    if completed?
      @rent = @rent*2
    end

    if @name == 'Old Kent Road'
      if @no_of_houses == 1
        @rent = 10
      elsif @no_of_houses == 2
        @rent = 30
      elsif @no_of_houses == 3
        @rent = 90
      elsif @no_of_houses == 4
        @rent = 160
      elsif hotel?
        @rent = 250
      end
    elsif @name == 'Whitechapel Road'
      if @no_of_houses == 1
        @rent = 20
      elsif @no_of_houses == 2
        @rent = 60
      elsif @no_of_houses == 3
        @rent = 180
      elsif @no_of_houses == 4
        @rent = 320
      elsif hotel?
        @rent = 450
      end
    elsif @name == 'The Angel Islington' || @name == 'Euston Road'
      if @no_of_houses == 1
        @rent = 30
      elsif @no_of_houses == 2
        @rent = 90
      elsif @no_of_houses == 3
        @rent = 270
      elsif @no_of_houses == 4
        @rent = 400
      elsif hotel?
        @rent = 550
      end
    elsif @name == 'Pentonville Road'
      if @no_of_houses == 1
        @rent = 40
      elsif @no_of_houses == 2
        @rent = 100
      elsif @no_of_houses == 3
        @rent = 300
      elsif @no_of_houses == 4
        @rent = 450
      elsif hotel?
        @rent = 600
      end
    elsif @name == 'Pall Mall' || @name == 'Whitehall'
      if @no_of_houses == 1
        @rent = 50
      elsif @no_of_houses == 2
        @rent = 150
      elsif @no_of_houses == 3
        @rent = 450
      elsif @no_of_houses == 4
        @rent = 625
      elsif hotel?
        @rent = 750
      end
    elsif @name == 'Northumberland Avenue'
      if @no_of_houses == 1
        @rent = 60
      elsif @no_of_houses == 2
        @rent = 180
      elsif @no_of_houses == 3
        @rent = 500
      elsif @no_of_houses == 4
        @rent = 700
      elsif hotel?
        @rent = 900
      end
    elsif @name == 'Bow Street' || @name == 'Marlborough Street'
      if @no_of_houses == 1
        @rent = 70
      elsif @no_of_houses == 2
        @rent = 200
      elsif @no_of_houses == 3
        @rent = 550
      elsif @no_of_houses == 4
        @rent = 750
      elsif hotel?
        @rent = 950
      end
    elsif @name == 'Vine Street'
      if @no_of_houses == 1
        @rent = 80
      elsif @no_of_houses == 2
        @rent = 220
      elsif @no_of_houses == 3
        @rent = 600
      elsif @no_of_houses == 4
        @rent = 800
      elsif hotel?
        @rent = 1000
      end
    elsif @name == 'The Strand' || @name == 'Fleet Street'
      if @no_of_houses == 1
        @rent = 90
      elsif @no_of_houses == 2
        @rent = 250
      elsif @no_of_houses == 3
        @rent = 700
      elsif @no_of_houses == 4
        @rent = 875
      elsif hotel?
        @rent = 1050
      end
    elsif @name == 'Trafalgar Square'
      if @no_of_houses == 1
        @rent = 100
      elsif @no_of_houses == 2
        @rent = 300
      elsif @no_of_houses == 3
        @rent = 750
      elsif @no_of_houses == 4
        @rent = 925
      elsif hotel?
        @rent = 1100
      end
    elsif @name == 'Leicester Square' || @name == 'Coventry Street'
      if @no_of_houses == 1
        @rent = 110
      elsif @no_of_houses == 2
        @rent = 330
      elsif @no_of_houses == 3
        @rent = 800
      elsif @no_of_houses == 4
        @rent = 975
      elsif hotel?
        @rent = 1150
      end
    elsif @name == 'Piccadilly'
      if @no_of_houses == 1
        @rent = 120
      elsif @no_of_houses == 2
        @rent = 360
      elsif @no_of_houses == 3
        @rent = 850
      elsif @no_of_houses == 4
        @rent = 1025
      elsif hotel?
        @rent = 1200
      end
    elsif @name == 'Regent Street' || @name == 'Oxford Street'
      if @no_of_houses == 1
        @rent = 130
      elsif @no_of_houses == 2
        @rent = 390
      elsif @no_of_houses == 3
        @rent = 900
      elsif @no_of_houses == 4
        @rent = 1100
      elsif hotel?
        @rent = 1275
      end
    elsif @name == 'Bond Street'
      if @no_of_houses == 1
        @rent = 150
      elsif @no_of_houses == 2
        @rent = 450
      elsif @no_of_houses == 3
        @rent = 1000
      elsif @no_of_houses == 4
        @rent = 1200
      elsif hotel?
        @rent = 1400
      end
    elsif @name == 'Park Lane'
      if @no_of_houses == 1
        @rent = 175
      elsif @no_of_houses == 2
        @rent = 500
      elsif @no_of_houses == 3
        @rent = 1100
      elsif @no_of_houses == 4
        @rent = 1300
      elsif hotel?
        @rent = 1500
      end
    elsif @name == 'Mayfair'
      if @no_of_houses == 1
        @rent = 200
      elsif @no_of_houses == 2
        @rent = 600
      elsif @no_of_houses == 3
        @rent = 1400
      elsif @no_of_houses == 4
        @rent = 1700
      elsif hotel?
        @rent = 2000
      end
    end
    @rent
  end


  def no_of_houses
    @no_of_houses
  end

  def house_price
    set = @colour_group
    if set == 'brown' || set == 'light_blue'
      @house_price = 50
    elsif set == 'pink' || set == 'orange'
      @house_price = 100
    elsif set == 'red' || set == 'yellow'
      @house_price = 150
    else
      @house_price = 200
    end
  end

  def buy_house(player)
    unless @no_of_houses == 4
      if player.funds >= @house_price
        player.funds -= @house_price
        @no_of_houses += 1
      else
        StandardError
      end
    end
    buy_hotel(player) if @no_of_houses == 4
  end

  def buy_hotel(player)
    if player.funds >= @house_price
      player.funds -= @house_price
      @no_of_houses = 0
      @hotel = true
    end
  end

  def hotel?
    @hotel
  end
end
